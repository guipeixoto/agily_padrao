<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('project-user', 'ProjectUserController');
    Route::resource('addresses', 'AddressesController');
    Route::resource('departaments', 'DepartamentsController');
    Route::resource('phones', 'PhonesController');
    Route::resource('projects', 'ProjectsController');
    Route::resource('users', 'UsersController');

    // para exemplo de uso sem Prettus
    Route::resource('texts', 'TextsController');
});
