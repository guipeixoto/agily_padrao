$(document).ready(function () {
    /**
     * Documentation for Bootstrap Datepicker
     * https://bootstrap-datepicker.readthedocs.io/en/latest/index.html
     */

    $('.datepicker').datepicker({
        autoclose: true,
        assumeNearbyYear: true,
        clearBtn: true,
        datesDisabled: [],
        daysOfWeekDisabled: [0],
        daysOfWeekHighlighted: [],
        enableOnReadonly: false,
        title: '',
        todayBtn: true,
        todayHighlight: true,
        weekStart: 1,
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
    });

    /**
     * Documentation for Datatables.net
     * https://datatables.net/manual/
     */
    $('.datatables').dataTable({
        searching: false,
        lengthChange: false,
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "select": {
                "rows": {
                    "_": "Selecionado %d linhas",
                    "0": "Nenhuma linha selecionada",
                    "1": "Selecionado 1 linha"
                }
            }
        }
    });

    $('.phone-mask').keyup(function () {
        setPhoneMask($(this).val());
    });

    $(".zipcode-mask").mask('00000-000');

    $('.select2').select2({
        placeholder: 'Selecione...'
    });
});

function setPhoneMask(phone) {
    if(phone.length === 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
        $('.phone-mask').mask('(00) 00000-0009');
    } else {
        $('.phone-mask').mask('(00) 0000-00009');
    }
}
