@extends('layouts.app')

@section('content')
    <div class="container-md">
        <div class="card">
            <form action="{{ route('users.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="card-header d-flex justify-content-between">
                    <h5>Atualizar Usuário</h5>
                    <a href="{{ route('users.index') }}" class="btn btn-secondary">Voltar</a>
                </div>

                <div class="card-body">
                    @include('layouts.validations')

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="name">Nome</label>
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name', $user->name) }}"
                                   required
                                   autocomplete="name"
                                   autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-3">
                            <label for="email">Email</label>
                            <input id="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{ old('email', $user->email) }}"
                                   required
                                   autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-3">
                            <label for="departament_id">Departamento</label>
                            <select name="departament_id"
                                    id="departament_id"
                                    class="form-control @error('departament_id') is-invalid @enderror"
                                    required
                                    autocomplete="departament_id">
                                <option value=""></option>
                                @foreach($dependencies['departaments'] as $departament)
                                    <option value="{{ $departament->id }}"
                                        {{ old('departament_id', $user->departament_id) == $departament->id ? 'selected' : '' }}>
                                        {{ $departament->name }}
                                    </option>
                                @endforeach
                            </select>

                            @error('departament_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-2">
                            <label for="birthday">Data Nacimento</label>
                            <input id="birthday"
                                   type="text"
                                   class="form-control datepicker @error('birthday') is-invalid @enderror"
                                   name="birthday"
                                   value="{{ old('birthday', $user->birthday) }}"
                                   required
                                   autocomplete="birthday">

                            @error('birthday')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password">Senha</label>
                            <input id="password"
                                   type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password"
                                   autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password-confirm">Confirmação de Senha</label>
                            <input id="password-confirm"
                                   type="password"
                                   class="form-control"
                                   name="password_confirmation"
                                   autocomplete="new-password">
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Atualizar</button>
                </div>
            </form>
        </div>

        {{--
            ################
            ## ENDEREÇOS ###
            ################
        --}}
        @include('addresses.index', ['model' => $user])

        {{--
            ################
            ### CONTATOS ###
            ################
        --}}
        @include('phones.index')
    </div>
@endsection
