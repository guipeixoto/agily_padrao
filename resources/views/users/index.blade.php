@extends('layouts.app')

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>Usuários <span class="badge badge-info">{{ count($users) }}</span></h5>
            <a href="{{ route('users.create') }}" class="btn btn-secondary">Adicionar Usuário</a>
        </div>

        <div class="card-body">
            <form action="{{ route('users.index') }}" method="GET">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="name">Nome</label>
                        <input type="text"
                               name="name"
                               id="name"
                               class="form-control"
                               autocomplete="name"
                               value="{{ request()->input('name') }}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="email">Email</label>
                        <input type="text"
                               name="email"
                               id="email"
                               class="form-control"
                               autocomplete="email"
                               value="{{ request()->input('email') }}">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="departament_name">Departamento</label>
                        <input type="text"
                               name="departament_name"
                               id="departament_name"
                               class="form-control"
                               autocomplete="departament_name"
                               value="{{ request()->input('departament_name') }}">
                    </div>

                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-outline-primary">Buscar</button>
                        <a href="{{ route('users.index') }}" class="btn btn-outline-secondary">Limpar</a>
                    </div>
                </div>
            </form>

            <table class="datatables table table-striped border-light mt-3">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Departamento</th>
                    <th>Data Nascimento</th>
                    <th class="text-center">Ações</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->departament->name }}</td>
                        <td>{{ $user->birthday }}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{ route('users.edit', $user->id) }}" class="mr-2 btn btn-sm btn-outline-primary">
                                <i class="far fa-edit"></i>
                            </a>

                            <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                @method('DELETE')
                                @csrf

                                <button class="btn btn-outline-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
