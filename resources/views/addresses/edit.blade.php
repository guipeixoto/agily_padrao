<form action="{{ route('addresses.update', $address->id) }}" method="POST">
    @method('PUT')
    @csrf

    <div class="row">
        <div class="form-group col-md-2">
            <label for="zipcode">
                CEP &nbsp;<i style="display: none;" class="fas fa-spinner zipcode-loader fa-pulse text-secondary"></i>
            </label>
            <input type="text"
                   required
                   name="zipcode"
                   id="zipcode"
                   value="{{ $address->zipcode ?? '' }}"
                   class="form-control zipcode-mask @error('zipcode') is-invalid @enderror">

            <small style="display: none;" class="text-danger zipcode-error font-weight-bold">Cep não encontrado</small>

            @error('zipcode')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message ?? '' !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-8">
            <label for="street">Logradouro</label>
            <input type="text"
                   required
                   name="street"
                   id="street"
                   value="{{ $address->street ?? '' }}"
                   class="form-control @error('street') is-invalid @enderror">

            @error('street')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-2">
            <label for="number">Número</label>
            <input type="number"
                   required
                   name="number"
                   id="number"
                   value="{{ $address->number ?? '' }}"
                   class="form-control @error('number') is-invalid @enderror">

            @error('number')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            <label for="district">Bairro</label>
            <input type="text"
                   required
                   name="district"
                   id="district"
                   value="{{ $address->district ?? '' }}"
                   class="form-control @error('district') is-invalid @enderror">

            @error('district')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-5">
            <label for="city">Cidade</label>
            <input type="text"
                   required
                   name="city"
                   id="city"
                   value="{{ $address->city ?? '' }}"
                   class="form-control @error('zipcode') is-invalid @enderror">

            @error('city')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-3">
            <label for="state">Estado</label>
            <select id="state" name="state" class="form-control @error('state') is-invalid @enderror">
                <option value="">Selecione...</option>
                <option value="AC" {{ $address->state == 'AC' ? 'selected' : '' }}>Acre</option>
                <option value="AL" {{ $address->state == 'AL' ? 'selected' : '' }}>Alagoas</option>
                <option value="AP" {{ $address->state == 'AP' ? 'selected' : '' }}>Amapá</option>
                <option value="AM" {{ $address->state == 'AM' ? 'selected' : '' }}>Amazonas</option>
                <option value="BA" {{ $address->state == 'BA' ? 'selected' : '' }}>Bahia</option>
                <option value="CE" {{ $address->state == 'CE' ? 'selected' : '' }}>Ceará</option>
                <option value="DF" {{ $address->state == 'DF' ? 'selected' : '' }}>Distrito Federal</option>
                <option value="ES" {{ $address->state == 'ES' ? 'selected' : '' }}>Espírito Santo</option>
                <option value="GO" {{ $address->state == 'GO' ? 'selected' : '' }}>Goiás</option>
                <option value="MA" {{ $address->state == 'MA' ? 'selected' : '' }}>Maranhão</option>
                <option value="MT" {{ $address->state == 'MT' ? 'selected' : '' }}>Mato Grosso</option>
                <option value="MS" {{ $address->state == 'MS' ? 'selected' : '' }}>Mato Grosso do Sul</option>
                <option value="MG" {{ $address->state == 'MG' ? 'selected' : '' }}>Minas Gerais</option>
                <option value="PA" {{ $address->state == 'PA' ? 'selected' : '' }}>Pará</option>
                <option value="PB" {{ $address->state == 'PB' ? 'selected' : '' }}>Paraíba</option>
                <option value="PR" {{ $address->state == 'PR' ? 'selected' : '' }}>Paraná</option>
                <option value="PE" {{ $address->state == 'PE' ? 'selected' : '' }}>Pernambuco</option>
                <option value="PI" {{ $address->state == 'PI' ? 'selected' : '' }}>Piauí</option>
                <option value="RJ" {{ $address->state == 'RJ' ? 'selected' : '' }}>Rio de Janeiro</option>
                <option value="RN" {{ $address->state == 'RN' ? 'selected' : '' }}>Rio Grande do Norte</option>
                <option value="RS" {{ $address->state == 'RS' ? 'selected' : '' }}>Rio Grande do Sul</option>
                <option value="RO" {{ $address->state == 'RO' ? 'selected' : '' }}>Rondônia</option>
                <option value="RR" {{ $address->state == 'RR' ? 'selected' : '' }}>Roraima</option>
                <option value="SC" {{ $address->state == 'SC' ? 'selected' : '' }}>Santa Catarina</option>
                <option value="SP" {{ $address->state == 'SP' ? 'selected' : '' }}>São Paulo</option>
                <option value="SE" {{ $address->state == 'SE' ? 'selected' : '' }}>Sergipe</option>
                <option value="TO" {{ $address->state == 'TO' ? 'selected' : '' }}>Tocantins</option>
                <option value="EX" {{ $address->state == 'EX' ? 'selected' : '' }}>Estrangeiro</option>
            </select>

            @error('state')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block">Atualizar</button>
    </div>
</form>
