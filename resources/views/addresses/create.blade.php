<form action="{{ route('addresses.store') }}" method="POST">
    @csrf

    <input type="hidden" name="user_id" value="{{ $user->id }}">

    <div class="row">
        <div class="form-group col-md-2">
            <label for="zipcode">
                CEP &nbsp;<i style="display: none;" class="fas fa-spinner zipcode-loader fa-pulse text-secondary"></i>
            </label>
            <input type="text"
                   required
                   name="zipcode"
                   id="zipcode"
                   class="form-control zipcode-mask @error('zipcode') is-invalid @enderror">

            <small style="display: none;" class="text-danger zipcode-error font-weight-bold">Cep não encontrado</small>

            @error('zipcode')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message ?? '' !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-8">
            <label for="street">Logradouro</label>
            <input type="text"
                   required
                   name="street"
                   id="street"
                   class="form-control @error('street') is-invalid @enderror">

            @error('street')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-2">
            <label for="number">Número</label>
            <input type="number"
                   required
                   name="number"
                   id="number"
                   class="form-control @error('number') is-invalid @enderror">

            @error('number')
                <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-5">
            <label for="district">Bairro</label>
            <input type="text"
                   required
                   name="district"
                   id="district"
                   class="form-control @error('district') is-invalid @enderror">

            @error('district')
                <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-5">
            <label for="city">Cidade</label>
            <input type="text"
                   required
                   name="city"
                   id="city"
                   class="form-control @error('zipcode') is-invalid @enderror">

            @error('city')
                <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-2">
            <label for="state">Estado</label>
            <select id="state" name="state" class="form-control @error('state') is-invalid @enderror">
                <option value="">Selecione...</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
                <option value="EX">Estrangeiro</option>
            </select>

            @error('state')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Incluir</button>
    </div>
</form>

<script type="application/javascript">
    $(document).ready(function () {
        $('#zipcode').keyup(function () {
            let zipcode = $(this).val();

            if (zipcode.length === 9) {
                $(".zipcode-loader").show();

                getCep(zipcode)
                    .then((response) => {
                        $(".zipcode-loader").hide();
                        $(".zipcode-error").hide();

                        $(this).removeClass('is-invalid');

                        setAddressFields(response);
                    })
                    .catch((error) => {
                        $(".zipcode-loader").hide();
                        $(".zipcode-error").show();

                        $(this).addClass('is-invalid');

                        clearAddressFields();
                    });
            }
        });
    });

    function setAddressFields(data) {
        $("#street").val(data.logradouro || data.street);
        $("#district").val(data.bairro || data.neighborhood);
        $("#city").val(data.localidade || data.city);
        $("#state").val(data.uf || data.state);
    }

    function clearAddressFields() {
        $("#street").val(null);
        $("#district").val(null);
        $("#city").val(null);
        $("#state").val(null);
    }
</script>
