<div class="card mt-5">
    <div class="card-header d-flex justify-content-between">
        <h5>Endereços</h5>
    </div>

    <div class="card-body">
        @include('addresses.create')

        <div class="mt-3">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>CEP</th>
                    <th>Logradouro</th>
                    <th>Número</th>
                    <th>Cidade</th>
                    <th>Bairro</th>
                    <th>Estado</th>
                    <th class="text-center">Ações</th>
                </tr>
                </thead>

                <tbody>
                @forelse($model->addresses as $address)
                    <tr>
                        <td class="zipcode-mask">{{ $address->zipcode }}</td>
                        <td>{{ $address->street }}</td>
                        <td>{{ $address->number }}</td>
                        <td>{{ $address->city }}</td>
                        <td>{{ $address->district }}</td>
                        <td>{{ $address->state }}</td>
                        <td class="d-flex justify-content-center">
                            <button type="button"
                                    data-id="{{ $address->id }}"
                                    class="mr-2 btn btn-sm btn-outline-primary"
                                    data-toggle="modal" data-target="#editAddressModal">
                                <i class="far fa-edit"></i>
                            </button>

                            <form action="{{ route('addresses.destroy', $address->id) }}" method="POST">
                                @method('DELETE')
                                @csrf

                                <button class="btn btn-outline-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7" class="text-center font-weight-bolder">Nenhum endereço cadastrado</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="editAddressContent"></div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function () {
        $('#editAddressModal').on('show.bs.modal', function (e) {
            $('.preloader').show();

            let id = $(e.relatedTarget).data('id');

            axios.get('/addresses/'+id+'/edit')
                .then((response) => {
                    $('.preloader').hide();

                    $('#editAddressContent').html(response.data);
                })
                .catch(() => {
                    $('.preloader').hide();
                });
        });
    });
</script>
