@extends('layouts.app')

@section('content')
    <div class="container-md">
        <div class="card">
            <form action="{{ route('departaments.update', $departament->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="card-header d-flex justify-content-between">
                    <h5>Atualizar Departamento</h5>
                    <a href="{{ route('departaments.index') }}" class="btn btn-secondary">Voltar</a>
                </div>

                <div class="card-body">
                    @include('layouts.validations')

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="name">Nome</label>
                            <input type="text"
                                   name="name"
                                   id="name"
                                   class="form-control @error('name') is-invalid @enderror"
                                   value="{{ $departament->name ?? old('name') }}"
                                   required>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Atualizar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
