@extends('layouts.app')

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>Departamentos <span class="badge badge-info">{{ count($departaments) }}</span></h5>
            <a href="{{ route('departaments.create') }}" class="btn btn-secondary">Adicionar Departamento</a>
        </div>

        <div class="card-body">
            <form action="{{ route('departaments.index') }}" method="GET">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="name">Nome</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ request()->input('name') }}">
                    </div>

                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-outline-primary">Buscar</button>
                        <a href="{{ route('departaments.index') }}" class="btn btn-outline-secondary">Limpar</a>
                    </div>
                </div>
            </form>

            <table class="datatables table table-striped border-light mt-3">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th class="text-center">Ações</th>
                </tr>
                </thead>

                <tbody>
                @foreach($departaments as $departament)
                    <tr>
                        <td>{{ $departament->id }}</td>
                        <td>{{ $departament->name }}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{ route('departaments.edit', $departament->id) }}" class="mr-2 btn btn-sm btn-outline-primary">
                                <i class="far fa-edit"></i>
                            </a>

                            <form action="{{ route('departaments.destroy', $departament->id) }}" method="POST">
                                @method('DELETE')
                                @csrf

                                <button class="btn btn-outline-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
