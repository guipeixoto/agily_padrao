<form action="{{ route('phones.update', $phone->id) }}" method="POST">
    @method('PUT')
    @csrf

    <div class="row">
        <div class="form-group col-md-6">
            <label for="phone_type_id">Tipo do Contato</label>
            <select name="phone_type_id"
                    id="phone_type_id"
                    class="form-control @error('phone_type_id') is-invalid @enderror"
                    required
                    autocomplete="phone_type_id">
                <option value=""></option>
                @foreach($phone_types as $phone_type)
                    <option value="{{ $phone_type->id }}"
                        {{ $phone->phone_type_id == $phone_type->id ? 'selected' : '' }}>
                        {{ $phone_type->name }}
                    </option>
                @endforeach
            </select>

            @error('phone_type_id')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-6">
            <label for="phone_number">Número</label>
            <input id="phone_number"
                   type="text"
                   class="form-control phone-mask @error('phone_number') is-invalid @enderror"
                   name="phone_number"
                   value="{{ $phone->number }}"
                   required
                   autocomplete="phone_number">

            @error('phone_number')
            <span class="invalid-feedback" role="alert">
                    <strong>{!! $message !!}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-block btn-success">Atualizar</button>
    </div>
</form>
