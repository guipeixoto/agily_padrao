<div class="card mt-5">
    <div class="card-header d-flex justify-content-between">
        <h5>Contatos</h5>
    </div>

    <div class="card-body">
        @include('phones.create')

        <div class="mt-3">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Tipo do Contato</th>
                    <th>Número</th>
                    <th class="text-center">Ações</th>
                </tr>
                </thead>

                <tbody>
                @forelse($user->phones as $phone)
                    <tr>
                        <td>{{ $phone->phoneType->name }}</td>
                        <td>
                            <span class="phone-mask">{{ $phone->number }}</span>
                        </td>
                        <td class="d-flex justify-content-center">
                            <button type="button"
                                    data-id="{{ $phone->id }}"
                                    class="mr-2 btn btn-sm btn-outline-primary"
                                    data-toggle="modal" data-target="#editContactModal">
                                <i class="far fa-edit"></i>
                            </button>

                            <form action="{{ route('phones.destroy', $phone->id) }}" method="POST">
                                @method('DELETE')
                                @csrf

                                <button class="btn btn-outline-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center font-weight-bolder">Nenhum contato cadastrado</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="editContactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="editContactContent"></div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function () {
        $('#editContactModal').on('show.bs.modal', function (e) {
            $('.preloader').show();

            let id = $(e.relatedTarget).data('id');

            axios.get('/phones/'+id+'/edit')
                .then((response) => {
                    $('.preloader').hide();

                    $('#editContactContent').html(response.data);
                })
                .catch(() => {
                    $('.preloader').hide();
                });
        });
    });
</script>
