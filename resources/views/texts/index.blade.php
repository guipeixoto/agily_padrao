@extends('layouts.app')

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>Textos <span class="badge badge-info">{{ count($texts) }}</span></h5>
            <a href="{{ route('texts.create') }}" class="btn btn-secondary">Adicionar Texto</a>
        </div>

        <div class="card-body">
            <table class="datatables table table-striped border-light mt-3">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Conteúdo</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($texts as $text)
                        <tr>
                            <td>{{ $text->id }}</td>
                            <td>{{ $text->name }}</td>
                            <td>{{ $text->content }}</td>
                            <td class="d-flex justify-content-center">
                                <a href="{{ route('texts.edit', $text->id) }}" class="mr-2 btn btn-sm btn-outline-primary">
                                    <i class="far fa-edit"></i>
                                </a>

                                <form action="{{ route('texts.destroy', $text->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf

                                    <button class="btn btn-outline-danger btn-sm">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
