@extends('layouts.app')

@section('content')
    <div class="container-md">
        <div class="card">
            <form action="{{ route('texts.store') }}" method="POST">
                @csrf

                <div class="card-header d-flex justify-content-between">
                    <h5>Adicionar Texto</h5>
                    <a href="{{ route('texts.index') }}" class="btn btn-secondary">Voltar</a>
                </div>

                <div class="card-body">
                    @include('layouts.validations')

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="name">Nome</label>
                            <input type="text"
                                   name="name"
                                   id="name"
                                   class="form-control @error('name') is-invalid @enderror"
                                   value="{{ old('name') }}"
                                   required>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="content">Conteúdo</label>
                            <textarea name="content"
                                      id="content"
                                      class="form-control @error('content') is-invalid @enderror"
                                      required
                                      rows="5">{{ old('content') }}</textarea>

                            @error('content')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
