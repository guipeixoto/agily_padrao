@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <p>Olá <strong>{{ Auth::user()->name }}</strong>, seja bem-vindo.</p>

                        <hr>

                        <p class="text-center">Escolha qual CRUD deseja acessar abaixo.</p>
                        <div class="list-group list-group-horizontal">
                            <a class="list-group-item list-group-item-action h5 text-center" href="{{ route('departaments.index') }}">
                                <u>Departamentos</u>
                            </a>
                            <a class="list-group-item list-group-item-action h5 text-center" href="{{ route('users.index') }}">
                                <u>Usuários</u>
                            </a>
                            <a class="list-group-item list-group-item-action h5 text-center" href="{{ route('projects.index') }}">
                                <u>Projetos</u>
                            </a>
                            <a class="list-group-item list-group-item-action h5 text-center" href="{{ route('texts.index') }}">
                                <u>Textos</u>
                            </a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
