@extends('layouts.app')

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5>Projetos <span class="badge badge-info">{{ count($projects) }}</span></h5>
            <a href="{{ route('projects.create') }}" class="btn btn-secondary">Adicionar Projeto</a>
        </div>

        <div class="card-body">
            <form action="{{ route('projects.index') }}" method="GET">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="name">Nome</label>
                        <input type="text"
                               name="name"
                               id="name"
                               class="form-control"
                               autocomplete="name"
                               value="{{ request()->input('name') }}">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="responsible_id">Responsável</label>
                        <select name="responsible_id"
                                id="responsible_id"
                                class="form-control select2 h-100"
                                autocomplete="responsible_id">
                            <option value=""></option>
                            @foreach($responsibles as $responsible)
                                <option value="{{ $responsible->id }}"
                                    {{ request()->input('responsible_id') == $responsible->id ? 'selected' : '' }}>
                                    {{ $responsible->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="start_date">Data Início</label>
                        <input type="text"
                               name="start_date"
                               id="start_date"
                               class="form-control datepicker"
                               autocomplete="start_date"
                               value="{{ request()->input('start_date') }}">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="end_date">Data Fim</label>
                        <input type="text"
                               name="end_date"
                               id="end_date"
                               class="form-control datepicker"
                               autocomplete="end_date"
                               value="{{ request()->input('end_date') }}">
                    </div>

                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-outline-primary">Buscar</button>
                        <a href="{{ route('projects.index') }}" class="btn btn-outline-secondary">Limpar</a>
                    </div>
                </div>
            </form>

            <table class="datatables table table-striped border-light mt-3">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Responsável</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th class="text-center">Ações</th>
                </tr>
                </thead>

                <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td>{{ $project->id }}</td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->responsible->name }}</td>
                        <td>{{ $project->start_date }}</td>
                        <td>{{ $project->end_date }}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{ route('projects.edit', $project->id) }}" class="mr-2 btn btn-sm btn-outline-primary">
                                <i class="far fa-edit"></i>
                            </a>

                            <form action="{{ route('projects.destroy', $project->id) }}" method="POST">
                                @method('DELETE')
                                @csrf

                                <button class="btn btn-outline-danger btn-sm">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
