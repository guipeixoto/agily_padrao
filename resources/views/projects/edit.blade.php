@extends('layouts.app')

@section('content')
    <div class="container-md">
        <div class="card">
            <form action="{{ route('projects.update', $project->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="card-header d-flex justify-content-between">
                    <h5>Atualizar Projeto</h5>
                    <a href="{{ route('projects.index') }}" class="btn btn-secondary">Voltar</a>
                </div>

                <div class="card-body">
                    @include('layouts.validations')

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="name">Nome</label>
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name', $project->name) }}"
                                   required
                                   autocomplete="name"
                                   autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-3">
                            <label for="start_date">Data Início</label>
                            <input id="start_date"
                                   type="text"
                                   class="form-control datepicker @error('start_date') is-invalid @enderror"
                                   name="start_date"
                                   value="{{ old('start_date', $project->start_date) }}"
                                   required
                                   autocomplete="start_date">

                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-3">
                            <label for="end_date">Data Fim</label>
                            <input id="end_date"
                                   type="text"
                                   class="form-control datepicker @error('end_date') is-invalid @enderror"
                                   name="end_date"
                                   value="{{ old('end_date', $project->end_date) }}"
                                   required
                                   autocomplete="end_date">

                            @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-3">
                            <label for="responsible_id">Responsável</label>
                            <select name="responsible_id"
                                    id="responsible_id"
                                    class="form-control @error('responsible_id') is-invalid @enderror"
                                    required
                                    autocomplete="responsible_id">
                                <option value=""></option>
                                @foreach($responsibles as $responsible)
                                    <option value="{{ $responsible->id }}"
                                        {{ old('responsible_id', $project->responsible_id) == $responsible->id ? 'selected' : '' }}>
                                        {{ $responsible->name }} - {{ $responsible->departament->name }}
                                    </option>
                                @endforeach
                            </select>

                            @error('responsible_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description">Descrição</label>
                            <textarea name="description"
                                      id="description"
                                      rows="4"
                                      class="form-control @error('description') is-invalid @enderror"
                                      autocomplete="description">{{ old('description', $project->description) }}</textarea>

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{!! $message !!}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Atualizar</button>
                </div>
            </form>
        </div>

        <project-user-component
            :users="{{ $users }}"
            :participants="{{ $project->users }}"
            project_id="{{ $project->id }}"></project-user-component>
    </div>
@endsection
