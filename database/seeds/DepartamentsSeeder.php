<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departaments')
            ->insert([
                [
                    'name' => 'Desenvolvimento',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'name' => 'Comercial',
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            ]);
    }
}
