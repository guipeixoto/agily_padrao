<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PhoneTypesSeeder::class);
         $this->call(DepartamentsSeeder::class);
         $this->call(UsersSeeder::class);
    }
}
