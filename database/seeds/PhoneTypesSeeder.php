<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhoneTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phone_types')
            ->insert([
                [
                    'name' => 'Celular',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'name' => 'Residencial',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'name' => 'Comercial',
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            ]);
    }
}
