<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PhoneTypeRepository;
use App\Models\PhoneType;
use App\Validators\PhoneTypeValidator;

/**
 * Class PhoneTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PhoneTypeRepositoryEloquent extends BaseRepository implements PhoneTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PhoneType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PhoneTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
