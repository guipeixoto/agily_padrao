<?php


namespace App\Repositories;


use App\Contracts\Repositories\TextRepository;
use App\Models\Text;

class TextRepositoryEloquent implements TextRepository
{
    /**
     * @param array $columns
     * @return Text[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function all($columns = ['*'])
    {
        return Text::get($columns);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function create(array $params)
    {
        return Text::create($params);
    }
}
