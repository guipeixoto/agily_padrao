<?php

namespace App\Repositories;

use App\Criteria\Projects\FilterStartEndDatesCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\ProjectRepository;
use App\Models\Project;
use App\Validators\ProjectValidator;

/**
 * Class ProjectRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    protected $fieldSearchable = [
        'name' => 'like',
        'responsible_id'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProjectValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function withFilterStartEndDateCriteria()
    {
        return $this->pushCriteria(
            new FilterStartEndDatesCriteria(request()->get('start_date', ''), request()->get('end_date', ''))
        );
    }
}
