<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\DepartamentRepository;
use App\Models\Departament;
use App\Validators\DepartamentValidator;

/**
 * Class DepartamentRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class DepartamentRepositoryEloquent extends BaseRepository implements DepartamentRepository
{
    protected $fieldSearchable = [
        'name' => 'like',
        'active'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Departament::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DepartamentValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
