<?php

namespace App\Criteria\Projects;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterStartEndDatesCriteria.
 *
 * @package namespace App\Criteria\Projects;
 */
class FilterStartEndDatesCriteria implements CriteriaInterface
{
    private $start_date;
    private $end_date;

    public function __construct($start_date, $end_date)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!empty($this->start_date)) {
            $this->start_date = Carbon::createFromFormat('d/m/Y', $this->start_date)->format('Y-m-d');
            $model = $model->where('start_date', '>=', $this->start_date);
        }

        if (!empty($this->end_date)) {
            $this->end_date = Carbon::createFromFormat('d/m/Y', $this->end_date)->format('Y-m-d');
            $model = $model->where('end_date', '<=', $this->end_date);
        }

        return $model;
    }
}
