<?php

namespace App\Criteria\Users;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterDepartamentRelationshipCriteria.
 *
 * @package namespace App\Criteria\Users;
 */
class FilterDepartamentRelationshipCriteria implements CriteriaInterface
{

    private $departament_name;

    public function __construct($departament_name)
    {
        $this->departament_name = $departament_name;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('departament', function ($query) {
            $query->where('name', 'like', "%$this->departament_name%");
        });
    }
}
