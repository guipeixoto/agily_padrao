<?php

namespace App\Presenters;

use App\Transformers\PhoneTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PhoneTypePresenter.
 *
 * @package namespace App\Presenters;
 */
class PhoneTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PhoneTypeTransformer();
    }
}
