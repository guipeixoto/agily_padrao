<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Contracts\Repositories\DepartamentRepository::class, \App\Repositories\DepartamentRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\ProjectRepository::class, \App\Repositories\ProjectRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\PhoneRepository::class, \App\Repositories\PhoneRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\PhoneTypeRepository::class, \App\Repositories\PhoneTypeRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\AddressRepository::class, \App\Repositories\AddressRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\TextRepository::class, \App\Repositories\TextRepositoryEloquent::class);
        //:end-bindings:
    }
}
