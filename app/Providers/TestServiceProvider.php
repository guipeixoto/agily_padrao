<?php

namespace App\Providers;

use App\Contracts\Repositories\TextRepository;
use App\Repositories\TextRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TextRepository::class, TextRepositoryEloquent::class);
    }
}
