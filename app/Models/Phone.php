<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Phone.
 *
 * @package namespace App\Models;
 */
class Phone extends BaseModel implements Transformable
{
    use TransformableTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'phone_type_id',
        'number',
        'active'
    ];

    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = str_replace(['(',')','-', ' '], '', $value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function phoneType()
    {
        return $this->belongsTo(PhoneType::class);
    }
}
