<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Address.
 *
 * @package namespace App\Models;
 */
class Address extends BaseModel implements Transformable
{
    use TransformableTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'addressable_id',
        'addressable_type',
        'zipcode',
        'street',
        'number',
        'city',
        'district',
        'state',
        'active'
    ];

    public function setZipcodeAttribute($value)
    {
        $this->attributes['zipcode'] = str_replace(['-'], '', $value);
    }

    public function addressable()
    {
        return $this->morphTo();
    }

}
