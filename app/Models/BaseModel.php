<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Address.
 *
 * @package namespace App\Models;
 */
class BaseModel extends Model
{
    use RevisionableTrait, RevisionableUpgradeTrait;

    protected $revisionCreationsEnabled = true;
}
