<?php

namespace App\Models;

use App\Contracts\Constants\PhoneTypesConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PhoneType.
 *
 * @package namespace App\Models;
 */
class PhoneType extends BaseModel implements Transformable, PhoneTypesConstants
{
    use TransformableTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

}
