<?php


namespace App\Services\Users;


use App\Contracts\Repositories\UserRepository;

class DestroyService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function run(int $id): bool
    {
        return $this->userRepository->delete($id);
    }
}
