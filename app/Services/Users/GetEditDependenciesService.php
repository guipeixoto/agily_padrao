<?php

namespace App\Services\Users;


use App\Contracts\Repositories\DepartamentRepository;
use App\Contracts\Repositories\PhoneTypeRepository;

class GetEditDependenciesService
{
    /**
     * @var DepartamentRepository
     */
    private $departamentRepository;
    /**
     * @var PhoneTypeRepository
     */
    private $phoneTypeRepository;

    public function __construct(DepartamentRepository $departamentRepository, PhoneTypeRepository $phoneTypeRepository)
    {
        $this->departamentRepository = $departamentRepository;
        $this->phoneTypeRepository = $phoneTypeRepository;
    }

    public function run(): array
    {
        $dependencies['departaments']   = $this->departamentRepository->all();
        $dependencies['phone_types']    = $this->phoneTypeRepository->all();

        return $dependencies;
    }
}

