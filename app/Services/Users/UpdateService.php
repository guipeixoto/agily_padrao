<?php


namespace App\Services\Users;


use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use App\Services\EncryptionPasswordService;
use Illuminate\Support\Facades\Hash;

class UpdateService
{
    private $userRepository;
    /**
     * @var EncryptionPasswordService
     */
    private $encryptionPasswordService;

    /**
     * UpdateService constructor.
     * @param UserRepository $userRepository
     * @param EncryptionPasswordService $encryptionPasswordService
     */
    public function __construct(UserRepository $userRepository, EncryptionPasswordService $encryptionPasswordService)
    {
        $this->userRepository = $userRepository;
        $this->encryptionPasswordService = $encryptionPasswordService;
    }

    /**
     * @param array $params
     * @param int $id
     * @return User
     */
    public function run(array $params, int $id): User
    {
        if (empty($params['password'])) {
            unset($params['password']);
        } else {
            $params['password'] = $this->encryptionPasswordService->run($params['password']);
        }

        return $this->userRepository->update($params, $id);
    }
}
