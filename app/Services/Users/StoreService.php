<?php


namespace App\Services\Users;


use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use App\Services\EncryptionPasswordService;
use Illuminate\Support\Facades\Hash;

class StoreService
{
    private $userRepository;
    /**
     * @var EncryptionPasswordService
     */
    private $encryptionPasswordService;

    /**
     * StoreService constructor.
     * @param UserRepository $userRepository
     * @param EncryptionPasswordService $encryptionPasswordService
     */
    public function __construct(UserRepository $userRepository, EncryptionPasswordService $encryptionPasswordService)
    {
        $this->userRepository = $userRepository;
        $this->encryptionPasswordService = $encryptionPasswordService;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function run(array $params)
    {
        $params['password'] = $this->encryptionPasswordService->run($params['password']);

        return $this->userRepository->create($params);
    }
}
