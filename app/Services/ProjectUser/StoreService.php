<?php

namespace App\Services\ProjectUser;


use App\Contracts\Repositories\ProjectRepository;
use App\Contracts\Repositories\UserRepository;
use App\Models\Project;
use App\Models\ProjectUserRepository;

class StoreService
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * StoreService constructor.
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     */
    public function __construct(ProjectRepository $projectRepository, UserRepository $userRepository)
    {
        $this->projectRepository = $projectRepository;
    }


    public function run(array $params)
    {
        $project = $this->projectRepository->find($params['project_id']);
        $project->users()->attach($params['user_id']);
        $project = $project->fresh('users');

        return $project->users;
    }
}
