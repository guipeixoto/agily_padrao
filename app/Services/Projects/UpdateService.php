<?php


namespace App\Services\Projects;


use App\Contracts\Repositories\ProjectRepository;
use App\Models\Project;
use Illuminate\Support\Facades\Hash;

class UpdateService
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param array $params
     * @param int $id
     * @return Project
     */
    public function run(array $params, int $id): Project
    {
        return $this->projectRepository->update($params, $id);
    }
}
