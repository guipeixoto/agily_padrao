<?php


namespace App\Services\Projects;


use App\Contracts\Repositories\ProjectRepository;

class DestroyService
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function run(int $id): bool
    {
        return $this->projectRepository->delete($id);
    }
}
