<?php

namespace App\Services\Projects;


use App\Contracts\Repositories\ProjectUserRepository;
use App\Contracts\Repositories\UserRepository;

class GetEditDependenciesService
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProjectUserRepository
     */
    private $projectUserRepository;

    /**
     * GetEditDependenciesService constructor.
     * @param UserRepository $userRepository
     * @param ProjectUserRepository $projectUserRepository
     */
    public function __construct(UserRepository $userRepository, ProjectUserRepository $projectUserRepository)
    {
        $this->userRepository = $userRepository;
        $this->projectUserRepository = $projectUserRepository;
    }

    /**
     * @param int $id
     * @return array
     */
    public function run(int $id): array
    {
        $relationsResponsibles = [
            'departament'
        ];

        $relationsProjectUsers = [
            'user'
        ];

        $dependencies['responsibles']   = $this->userRepository->with($relationsResponsibles)->all();
        $dependencies['users']          = clone $dependencies['responsibles'];
        $dependencies['projectUsers']   = $this->projectUserRepository->with($relationsProjectUsers)->all();

        return $dependencies;
    }
}
