<?php


namespace App\Services\Projects;


use App\Contracts\Repositories\ProjectRepository;
use App\Contracts\Repositories\UserRepository;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class StoreService
{

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param array $params
     * @return Project
     */
    public function run(array $params): Project
    {
        return $this->projectRepository->create($params);
    }
}
