<?php

namespace App\Services\Phones;


use App\Contracts\Repositories\PhoneRepository;
use App\Models\Phone;

class CreateService
{
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;

    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }

    public function run(array $params): Phone
    {
        $params['number'] = $params['phone_number'];

        return $this->phoneRepository->create($params);
    }
}

