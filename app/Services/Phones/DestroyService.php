<?php

namespace App\Services\Phones;


use App\Contracts\Repositories\PhoneRepository;

class DestroyService
{
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;

    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }

    public function run(int $id): bool
    {
        return $this->phoneRepository->delete($id);
    }
}

