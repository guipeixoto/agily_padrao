<?php

namespace App\Services\Phones;


use App\Contracts\Repositories\PhoneRepository;
use App\Models\Phone;

class UpdateService
{
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;

    /**
     * UpdateService constructor.
     * @param PhoneRepository $phoneRepository
     */
    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }

    /**
     * @param array $params
     * @param int $id
     * @return Phone
     */
    public function run(array $params, int $id): Phone
    {
        $params['number'] = $params['phone_number'];

        return $this->phoneRepository->update($params, $id);
    }
}
