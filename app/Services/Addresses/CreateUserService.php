<?php

namespace App\Services\Addresses;


use App\Contracts\Repositories\AddressRepository;
use App\Contracts\Repositories\UserRepository;
use App\Models\Address;

class CreateUserService
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * CreateUserService constructor.
     * @param AddressRepository $addressRepository
     * @param UserRepository $userRepository
     */
    public function __construct(AddressRepository $addressRepository, UserRepository $userRepository)
    {
        $this->addressRepository = $addressRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $params
     * @return Address
     */
    public function run(array $params): Address
    {
        $user = $this->userRepository->find($params['user_id']);

        return $user->addresses()->create($params);
    }
}

