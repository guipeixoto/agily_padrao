<?php

namespace App\Services\Addresses;


use App\Contracts\Repositories\AddressRepository;

class DestroyService
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * DestroyService constructor.
     * @param AddressRepository $addressRepository
     */
    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function run(int $id): bool
    {
        return $this->addressRepository->delete($id);
    }
}

