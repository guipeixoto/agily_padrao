<?php

namespace App\Services\Addresses;


use App\Contracts\Repositories\AddressRepository;
use App\Models\Address;

class UpdateService
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * UpdateService constructor.
     * @param AddressRepository $addressRepository
     */
    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param array $params
     * @param int $id
     * @return Address
     */
    public function run(array $params, int $id): Address
    {
        return $this->addressRepository->update($params, $id);
    }
}
