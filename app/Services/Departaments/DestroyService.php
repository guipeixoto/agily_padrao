<?php


namespace App\Services\Departaments;


use App\Contracts\Repositories\DepartamentRepository;

class DestroyService
{
    private $departamentRepository;

    public function __construct(DepartamentRepository $departamentRepository)
    {
        $this->departamentRepository = $departamentRepository;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function run(int $id): bool
    {
        return $this->departamentRepository->delete($id);
    }
}
