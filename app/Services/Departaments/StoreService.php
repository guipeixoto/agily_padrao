<?php


namespace App\Services\Departaments;


use App\Contracts\Repositories\DepartamentRepository;
use App\Models\Departament;

class StoreService
{
    private $departamentRepository;

    public function __construct(DepartamentRepository $departamentRepository)
    {
        $this->departamentRepository = $departamentRepository;
    }

    /**
     * @param array $params
     * @return Departament
     */
    public function run(array $params): Departament
    {
        return $this->departamentRepository->create($params);
    }
}
