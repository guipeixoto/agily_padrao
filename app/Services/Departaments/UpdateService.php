<?php


namespace App\Services\Departaments;


use App\Contracts\Repositories\DepartamentRepository;
use App\Models\Departament;

class UpdateService
{
    private $departamentRepository;

    public function __construct(DepartamentRepository $departamentRepository)
    {
        $this->departamentRepository = $departamentRepository;
    }

    /**
     * @param array $params
     * @param int $id
     * @return Departament
     */
    public function run(array $params, int $id): Departament
    {
        return $this->departamentRepository->update($params, $id);
    }
}
