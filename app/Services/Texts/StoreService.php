<?php

namespace App\Services\Texts;


use App\Contracts\Repositories\TextRepository;
use App\Models\Text;

class StoreService
{
    /**
     * @var TextRepository
     */
    private $textRepository;

    /**
     * StoreService constructor.
     * @param TextRepository $textRepository
     */
    public function __construct(TextRepository $textRepository)
    {
        $this->textRepository = $textRepository;
    }

    public function run(array $params): Text
    {
        return $this->textRepository->create($params);
    }
}
