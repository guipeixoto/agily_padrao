<?php

namespace App\Services;


use Illuminate\Support\Facades\Hash;

class EncryptionPasswordService
{
    /**
     * @param string $password
     * @return string
     */
    public function run(string $password): string
    {
        // cria uma hash do password passado como parâmetro e retorna
        return Hash::make($password);
    }
}
