<?php


namespace App\Contracts\Constants;

/**
 * Interface PhoneTypesConstants
 * @package App\Contracts\Constants
 */
interface PhoneTypesConstants
{
    const IS_CELULAR      = 1;
    const IS_RESIDENCIAL  = 2;
    const IS_COMERCIAL    = 3;
}
