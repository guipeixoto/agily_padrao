<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AddressableRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface AddressableRepository extends RepositoryInterface
{
    //
}
