<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DepartamentRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface DepartamentRepository extends RepositoryInterface
{
    //
}
