<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface ProjectRepository extends RepositoryInterface
{
    /**
     * @return $this
     */
    public function withFilterStartEndDateCriteria();
}
