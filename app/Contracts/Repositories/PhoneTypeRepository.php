<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhoneTypeRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface PhoneTypeRepository extends RepositoryInterface
{
    //
}
