<?php


namespace App\Contracts\Repositories;


interface TextRepository
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param array $params
     * @return mixed
     */
    public function create(array $params);
}
