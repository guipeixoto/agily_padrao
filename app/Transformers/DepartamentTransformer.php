<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Departament;

/**
 * Class DepartamentTransformer.
 *
 * @package namespace App\Transformers;
 */
class DepartamentTransformer extends TransformerAbstract
{
    /**
     * Transform the Departament entity.
     *
     * @param \App\Models\Departament $model
     *
     * @return array
     */
    public function transform(Departament $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
