<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PhoneType;

/**
 * Class PhoneTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class PhoneTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the PhoneType entity.
     *
     * @param \App\Models\PhoneType $model
     *
     * @return array
     */
    public function transform(PhoneType $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
