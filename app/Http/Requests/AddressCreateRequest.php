<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zipcode'           => 'required',
            'street'            => 'required',
            'number'            => 'required',
            'city'              => 'required',
            'district'          => 'required',
            'state'             => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'zipcode'   => '<strong>CEP</strong>',
            'street'    => '<strong>rua</strong>',
            'number'    => '<strong>número</strong>',
            'city'      => '<strong>cidade</strong>',
            'district'  => '<strong>bairro</strong>',
            'state'     => '<strong>estado</strong>'
        ];
    }
}
