<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'responsible_id' => 'required|numeric',
            'name' => 'required|min:3',
            'start_date' => 'required|date_format:d/m/Y',
            'end_date' => 'required|date_format:d/m/Y|after_or_equal:start_date',
        ];
    }

    public function attributes()
    {
        return [
            'responsible_id' => '<strong>responsável</strong>',
            'start_date' => '<strong>data início</strong>',
            'end_date' => '<strong>data fim</strong>'
        ];
    }
}
