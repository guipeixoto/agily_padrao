<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departament_id' => 'required|numeric',
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'birthday' => 'required|date',
            'password' => 'required|confirmed'
        ];
    }

    public function attributes()
    {
        return [
            'departament_id' => '<strong>departamento</strong>',
            'email' => '<strong>email</strong>',
            'password' => '<strong>senha</strong>',
            'birthday' => '<strong>data de nascimento</strong>',
//            'name' => '<strong>NOME DO USUÁRIO</strong>'
        ];
    }
}
