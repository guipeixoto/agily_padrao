<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'user_id'       => 'required|numeric',
//            'phone_type_id' => 'required|numeric',
//            'phone_number'  => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'user_id'       => '<strong>usuário</strong>',
            'phone_type_id' => '<strong>tipo de contato</strong>',
            'phone_number'  => '<strong>número</strong>',
        ];
    }
}
