<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|numeric|unique:project_user,project_id,NULL,NULL,user_id,'.request()->user_id,
            'user_id'    => 'required|numeric|unique:phones,user_id,NULL,NULL,project_id,'.request()->project_id,
        ];
    }

    public function attributes()
    {
        return [
            'project_id' => '<strong>projeto</strong>',
            'user_id' => '<strong>usuário</strong>'
        ];
    }
}
