<?php

namespace App\Http\Middleware;

use Closure;

class BuilderSearchCriteria
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $queryParams = http_build_query($request->all(), '', ';');

        if (!empty($queryParams)) {
            $searchCritetia = str_replace('=', ':', $queryParams);
            $request->request->add(['search' => $searchCritetia]);
            $request->request->add(['searchJoin' => 'and']);
        }

        return $next($request);
    }
}
