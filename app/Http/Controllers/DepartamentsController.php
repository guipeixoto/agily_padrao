<?php

namespace App\Http\Controllers;

use App\Services\Departaments\DestroyService;
use App\Services\Departaments\StoreService;
use App\Services\Departaments\UpdateService;
use Illuminate\Http\Request;

use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DepartamentCreateRequest;
use App\Http\Requests\DepartamentUpdateRequest;
use App\Contracts\Repositories\DepartamentRepository;

/**
 * Class DepartamentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class DepartamentsController extends Controller
{

    /**
     * @var DepartamentRepository
     */
    private $departamentRepository;


    public function __construct(DepartamentRepository $departamentRepository)
    {
        parent::__construct();

        $this->departamentRepository = $departamentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $departaments = $this->departamentRepository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $departaments,
            ]);
        }

        return view('departaments.index', compact('departaments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('departaments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreService $storeService
     * @param DepartamentCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     *
     */
    public function store(StoreService $storeService, DepartamentCreateRequest $request)
    {
        try {

            $departament = $storeService->run($request->all());

            $response = [
                'message' => 'Departamento criado.',
                'data'    => $departament->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()
                ->route('departaments.edit', $departament->id)
                ->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $departament = $this->departamentRepository->find($id);

        return view('departaments.edit', compact('departament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateService $updateService
     * @param DepartamentUpdateRequest $request
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     */
    public function update(UpdateService $updateService, DepartamentUpdateRequest $request, $id)
    {
        try {

            $departament = $updateService->run($request->all(), $id);

            $response = [
                'message' => 'Departamento atualizado.',
                'data'    => $departament->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyService $destroyService
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(DestroyService $destroyService, $id)
    {
        $deleted = $destroyService->run($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Departamento excluído.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Departamento excluído.');
    }
}
