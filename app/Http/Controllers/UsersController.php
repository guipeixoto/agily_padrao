<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\DepartamentRepository;
use App\Contracts\Repositories\UserRepository;
use App\Services\Users\DestroyService;
use App\Services\Users\GetEditDependenciesService;
use App\Services\Users\StoreService;
use App\Services\Users\UpdateService;

use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $relations = [
            'departament'
        ];

        $users = $this
            ->userRepository
            ->withFilterDepartamentRelationshipCriteria()
            ->with($relations)
            ->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $users,
            ]);
        }

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param DepartamentRepository $departamentRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create(DepartamentRepository $departamentRepository)
    {
        $departaments = $departamentRepository->all();

        return view('users.create', compact('departaments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreService $storeService
     * @param UserCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     *
     */
    public function store(StoreService $storeService, UserCreateRequest $request)
    {
        try {

            $user = $storeService->run($request->all());

            $response = [
                'message' => 'Usuário criado.',
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()
                ->route('users.edit', $user->id)
                ->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param GetEditDependenciesService $editDependenciesService
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(GetEditDependenciesService $editDependenciesService, $id)
    {
        $relations = [
            'addresses',
            'phones.phoneType',
        ];

        $user = $this->userRepository->with($relations)->find($id);

        $dependencies = $editDependenciesService->run();

        return view('users.edit',
            compact('user', 'dependencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateService $updateService
     * @param UserUpdateRequest $request
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     */
    public function update(UpdateService $updateService, UserUpdateRequest $request, $id)
    {
        try {

            $user = $updateService->run($request->all(), $id);

            $response = [
                'message' => 'Usuário atualizado.',
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()
                ->back()
                ->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyService $destroyService
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(DestroyService $destroyService, $id)
    {
        $deleted = $destroyService->run($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Usuário excluído.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Usuário excluído.');
    }
}
