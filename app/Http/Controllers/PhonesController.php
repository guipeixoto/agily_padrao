<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\PhoneTypeRepository;
use App\Services\Phones\CreateService;
use App\Services\Phones\DestroyService;
use App\Services\Phones\UpdateService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PhoneCreateRequest;
use App\Http\Requests\PhoneUpdateRequest;
use App\Contracts\Repositories\PhoneRepository;
use App\Validators\PhoneValidator;

/**
 * Class PhonesController.
 *
 * @package namespace App\Http\Controllers;
 */
class PhonesController extends Controller
{
    /**
     * @var PhoneRepository
     */
    private $phoneRepository;

    /**
     * PhonesController constructor.
     *
     * @param PhoneRepository $phoneRepository
     */
    public function __construct(PhoneRepository $phoneRepository)
    {
        parent::__construct();

        $this->phoneRepository = $phoneRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateService $createService
     * @param PhoneCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     */
    public function store(CreateService $createService, PhoneCreateRequest $request)
    {
        try {

            $phone = $createService->run($request->all());

            $response = [
                'message' => 'Contato criado.',
                'data'    => $phone->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * @param PhoneTypeRepository $phoneTypeRepository
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(PhoneTypeRepository $phoneTypeRepository, $id)
    {
        $phone = $this->phoneRepository->find($id);

        $phone_types = $phoneTypeRepository->all();

        return view('phones.edit', compact('phone', 'phone_types'));
    }

    /**
     * @param UpdateService $updateService
     * @param PhoneUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(UpdateService $updateService, PhoneUpdateRequest $request, $id)
    {
        try {

            $phones = $updateService->run($request->all(), $id);

            $response = [
                'message' => 'Endereço atualizado.',
                'data'    => $phones->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyService $destroyService
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(DestroyService $destroyService, $id)
    {
        $deleted = $destroyService->run($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Contato excluído.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Contato excluído.');
    }
}
