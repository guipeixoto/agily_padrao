<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\TextRepository;
use App\Http\Requests\TextCreateRequest;
use App\Services\Texts\StoreService;
use Illuminate\Http\Request;

class TextsController extends Controller
{
    /**
     * @var TextRepository
     */
    private $textRepository;

    /**
     * TextsController constructor.
     * @param TextRepository $textRepository
     */
    public function __construct(TextRepository $textRepository)
    {
        parent::__construct();

        $this->textRepository = $textRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $texts = $this->textRepository->all();

        return view('texts.index', compact('texts'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('texts.create');
    }

    /**
     * @param StoreService $storeService
     * @param TextCreateRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(StoreService $storeService, TextCreateRequest $request)
    {
        $text = $storeService->run($request->all());

        $response = [
            'message' => 'Texto criado.',
            'data'    => $text->toArray(),
        ];

        if ($request->wantsJson()) {
            return response()->json($response);
        }

        return redirect()
            ->route('texts.index')
            ->with('message', $request['message']);
    }
}
