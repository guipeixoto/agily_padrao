<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressCreateRequest;
use App\Http\Requests\AddressUpdateRequest;
use App\Services\Addresses\CreateUserService;
use App\Services\Addresses\DestroyService;
use App\Services\Addresses\UpdateService;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Contracts\Repositories\AddressRepository;

/**
 * Class AddressesController.
 *
 * @package namespace App\Http\Controllers;
 */
class AddressesController extends Controller
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * AddressablesController constructor.
     * @param AddressRepository $addressRepository
     */
    public function __construct(AddressRepository $addressRepository)
    {
        parent::__construct();

        $this->addressRepository = $addressRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserService $createUserService
     * @param AddressCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     */
    public function store(CreateUserService $createUserService, AddressCreateRequest $request)
    {
        try {

            $address = $createUserService->run($request->all());

            $response = [
                'message' => 'Endereço criado.',
                'data'    => $address->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $address = $this->addressRepository->find($id);

        return view('addresses.edit', compact('address'));
    }

    /**
     * @param UpdateService $updateService
     * @param AddressUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(UpdateService $updateService, AddressUpdateRequest $request, $id)
    {
        try {

            $address = $updateService->run($request->all(), $id);

            $response = [
                'message' => 'Endereço atualizado.',
                'data'    => $address->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyService $destroyService
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(DestroyService $destroyService, $id)
    {
        $deleted = $destroyService->run($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Endereço excluído.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Endereço excluído.');
    }
}
