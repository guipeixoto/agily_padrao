<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\ProjectRepository;
use App\Contracts\Repositories\UserRepository;
use App\Services\Projects\DestroyService;
use App\Services\Projects\StoreService;
use App\Services\Projects\UpdateService;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ProjectCreateRequest;
use App\Http\Requests\ProjectUpdateRequest;

/**
 * Class ProjectsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProjectsController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        parent::__construct();

        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(UserRepository $userRepository)
    {
        $relations = [
            'responsible'
        ];

        $projects = $this
            ->projectRepository
            ->withFilterStartEndDateCriteria()
            ->with($relations)
            ->all();

        $responsibles = $userRepository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $projects,
            ]);
        }

        return view('projects.index', compact('projects', 'responsibles'));
    }

    /**
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(UserRepository $userRepository)
    {
        $responsibles_relations = [
            'departament'
        ];

        $responsibles = $userRepository->with($responsibles_relations)->all();

        return view('projects.create', compact('responsibles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreService $storeService
     * @param ProjectCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(StoreService $storeService, ProjectCreateRequest $request)
    {
        try {

            $project = $storeService->run($request->all());

            $response = [
                'message' => 'Projeto criado.',
                'data'    => $project->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()
                ->route('projects.edit', $project->id)
                ->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserRepository $userRepository
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(UserRepository $userRepository, $id)
    {
        $projectRelations = [
            'users'
        ];

        $responsibleRelations = [
            'departament'
        ];

        $project = $this->projectRepository->with($projectRelations)->find($id);

        $responsibles = $userRepository->with($responsibleRelations)->all();
        $users = clone $responsibles;


        return view('projects.edit', compact('project', 'responsibles', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateService $updateService
     * @param ProjectUpdateRequest $request
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     */
    public function update(UpdateService $updateService, ProjectUpdateRequest $request, $id)
    {
        try {

            $project = $updateService->run($request->all(), $id);

            $response = [
                'message' => 'Projeto atualizado.',
                'data'    => $project->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyService $destroyService
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(DestroyService $destroyService, $id)
    {
        $deleted = $destroyService->run($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Projeto excluído.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Projeto excluído.');
    }
}
