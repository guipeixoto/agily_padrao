<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PhoneTypeCreateRequest;
use App\Http\Requests\PhoneTypeUpdateRequest;
use App\Contracts\Repositories\PhoneTypeRepository;
use App\Validators\PhoneTypeValidator;

/**
 * Class PhoneTypesController.
 *
 * @package namespace App\Http\Controllers;
 */
class PhoneTypesController extends Controller
{
    /**
     * @var PhoneTypeRepository
     */
    protected $repository;

    /**
     * @var PhoneTypeValidator
     */
    protected $validator;

    /**
     * PhoneTypesController constructor.
     *
     * @param PhoneTypeRepository $repository
     * @param PhoneTypeValidator $validator
     */
    public function __construct(PhoneTypeRepository $repository, PhoneTypeValidator $validator)
    {
        parent::__construct();

        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $phoneTypes = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $phoneTypes,
            ]);
        }

        return view('phoneTypes.index', compact('phoneTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PhoneTypeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PhoneTypeCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $phoneType = $this->repository->create($request->all());

            $response = [
                'message' => 'PhoneType created.',
                'data'    => $phoneType->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phoneType = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $phoneType,
            ]);
        }

        return view('phoneTypes.show', compact('phoneType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phoneType = $this->repository->find($id);

        return view('phoneTypes.edit', compact('phoneType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PhoneTypeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PhoneTypeUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $phoneType = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'PhoneType updated.',
                'data'    => $phoneType->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'PhoneType deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'PhoneType deleted.');
    }
}
