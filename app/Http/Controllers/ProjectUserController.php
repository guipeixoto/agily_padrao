<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectUserCreateRequest;
use App\Services\ProjectUser\StoreService;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectUserController extends Controller
{
    /**
     * @param StoreService $storeService
     * @param ProjectUserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(StoreService $storeService, ProjectUserCreateRequest $request)
    {
        try {

            $projectUsers = $storeService->run($request->all());

            $response = [
                'message' => 'Usuário incluso.',
                'data'    => $projectUsers->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()
                ->back()
                ->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }
}
